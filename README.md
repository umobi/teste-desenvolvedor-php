![Umobi](https://umobi.com.br/images/svg/logo-color.svg)

> Esse teste é público. Todos os interessados que o fizerem receberão um feedback da nossa equipe. 

## Teste para candidatos à vaga de desenvolvedor PHP

Somos um time de desenvolvimento de Software com múltiplas expertises e vários projetos em manutenção e desenvolvimento contínuo. Nosso portfólio pode ser encontrado no link:
[Umobi](https://umobi.com.br/portfolio)

## Quais tecnologias utilizamos?

Nosso time é composto por desenvolvedores com mútiplas especialidades

* Angular
* Ionic
* PHP
* Laravel
* Swift
* Objective C
* Java
* Flutter
* Kotlin
* Docker
* Unix/Linux

Você não é obrigado a conhecer todas as tecnologias acima mas é um diferencial ter o mínimo de experiência em algum dos citados.

*Vamos ao teste!*

## Seus objetivos são

- O teste consiste no desenvolvimento de uma Agenda, ou seja, um cadastro de contatos contendo nome e telefone. A aplicação deve conter no mínimo 2 telas, sendo uma a tela de listagem de contatos, e a outra a tela de detalhes do contato. Deve ter a possibilidade de **Adicionar**, **Remover** e **Editar** o contato.

## Stack para ser utilizada

* PHP
* Html/css
* Composer ( opcional )
* Algum Framework ( Laravel, CodeIgniter, Zend, Yii, CakePHP, Phalcon )
* Banco de Dados Mysql

### Leia, entenda, e tire todas as dúvidas referentes ao teste [aqui](https://bitbucket.org/umobi/teste-desenvolvedor-php/issues?status=new&status=open)

> Faça uma estimativa de horas para o desenvolvimento e envie um email com o título [Teste Dev PHP] Estimativa para contato@umobi.com.br
>
> Quando finalizar o teste, publique tudo no seu Bitbucket ou Github e envie um email com o título [Teste Dev PHP] Finalizado para contato@umobi.com.br

> **Importante:** Usamos o mesmo teste para todos os níveis de desenvolvedor: **trainee**, **junior**, **pleno** ou **senior**, mas procuramos adequar nossa exigência na avaliação com cada um desses níveis sem, por exemplo, exigir excelência de quem está começando :-D

### O Que Avaliamos

* Qualidade de código
* Código limpo
* Simplicidade
* Lógica de programação
* Conceitos de orientação a objetos
* Otimização do código implementado
* Organização e padrão de Commits

### Ganhe Bônus

* Utilize laravel para desenvolver o Backend.
* Utilize Angular, React ou Vue para desenvolver o Frontend
* Utilize as Guidelines do Material Design para Orientação visual ([Material Design](https://material.io/develop/web/))
* Utilize o padrão RESTFul para o desenvolvimento da API.
* Utilize algum padrão de resposta JSON.

**Boa sorte!**
